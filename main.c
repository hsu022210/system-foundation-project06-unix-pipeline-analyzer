#include<stdio.h>
#include<stdlib.h>
#include<sys/types.h>
#include<sys/wait.h>
#include<unistd.h>
#include <string.h>

int main(int argc, char **argv)
{
    pid_t id;
    int fds[2];
    int fds2[2];
    int i = 0;
    char buf[32];
    int byte_count = 0;
    int line_count = 0;
    int is_binary = 0;

    pipe(fds);
    pipe(fds2);

    char *outer_arr[argc][argc];
    int row = 0;

    for (i = 1; i < argc; i++) {
        int col = 0;

        while(strcmp(argv[i], "|") != 0){
            outer_arr[row][col] = malloc(strlen(argv[i]));
            outer_arr[row][col] = argv[i];

            col++;
            i++;

            if (i == argc){
                break;
            }
        }
        outer_arr[row][col] = '\0';
        row++;
    }

    FILE *f;
    f = fopen("pa.log", "w");

    /* Fork first process */
    id = fork();

    if (id < 0) {
        printf("fork() for first process failed\n");
        exit(-1);
    }

    if (id == 0) {

        /* Close read end of pipe */
        close(fds[0]);

        /* Close stdout */
        close(1);

        /* Dup write end of pipe */
        dup(fds[1]);

        /* Close extra write end of pipe */
        close(fds[1]);

        close(fds[0]);
        close(fds[1]);
        close(fds2[0]);
        close(fds2[1]);

        if (execvp(outer_arr[0][0], outer_arr[0]) < 0) {
            write(2, "execvp() failed for prog1\n", 27);
            exit(-1);
        }

        /* We will never reach this point */
    }

    /* Fork second process */

    id = fork();

    if (id < 0) {
        printf("fork() for second process failed\n");
        exit(-1);
    }

    if (id == 0) {

        f = fopen("pa.log", "a");

        char *log_text = malloc(sizeof(char)*1000);
        strcat(log_text, "[1]");
        strcat(log_text, outer_arr[0][0]);
        strcat(log_text, " -> ");
        strcat(log_text, outer_arr[1][0]);
        strcat(log_text, "\n");

        close(fds[1]);
        close(fds2[0]);
        while (read(fds[0], buf, 1) > 0){
            byte_count++;

            if (buf[0] == '\n'){
                line_count++;
            }

            if (((int)buf[0]) > 127){
                is_binary = 1;
            }
            write(fds2[1], buf, 1);
        }

        close(fds[0]);

        close(fds2[1]);

        char str_bytes[32];
        char str_lines[32];
        sprintf(str_lines, "%d", line_count);
        sprintf(str_bytes, "%d", byte_count);

        strcat(log_text, str_bytes);
        strcat(log_text, " bytes\n");
        strcat(log_text, str_lines);
        strcat(log_text, " lines\n");

        if (is_binary == 0){
            strcat(log_text, "ASCII data\n");
        } else{
            strcat(log_text, "BINARY data\n");
        }

        fprintf(f, "%s", log_text);
        fclose(f);

        close(fds[0]);
        close(fds[1]);
        close(fds2[0]);
        close(fds2[1]);

        exit(0);
    }

    id = fork();

    if (id < 0) {
        printf("fork() for third process failed\n");
        exit(-1);
    }

    if (id == 0) {
        /* Close write end of pipe */
        close(fds2[1]);

        close(0);
        dup(fds2[0]);

        /* Close extra read end of pipe */
        close(fds2[0]);

        close(fds[0]);
        close(fds[1]);
        close(fds2[0]);
        close(fds2[1]);

        if (execvp(outer_arr[1][0], outer_arr[1]) < 0) {
            write(2, "execvp() failed for prog3\n", 27);
            exit(-1);
        }
    }

    /* Need to close both ends of pipe in parent */
    close(fds[0]);
    close(fds[1]);
    close(fds2[0]);
    close(fds2[1]);

    id = wait(NULL);
    id = wait(NULL);
    id = wait(NULL);

    return 0;
}
