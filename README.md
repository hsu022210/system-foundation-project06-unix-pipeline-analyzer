Supports 1 pipeline with two pipes(two processes), make sure to add a \ before the pipe operator.

For example, `./main seq 100000 \| wc -l -c`, pa.log will be created in the same folder.
